# FastAPI Trivia Game

The purpose of this project is to display understanding of FastAPI through creation of an interactive trivia game.

## Running the Program

After cloning the repo from git lab to your local computer, please run the following commands to run the project. After the following commands have been run, create data to use for this project via the forms on the website. This project uses React, you may open the project in your browser at http://localhost:3000/

* docker volume create trivia-game
* docker-compose build
* docker-compose up